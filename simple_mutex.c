#include <stdio.h>
#include <pthread.h>
#include <stdbool.h>
#include <unistd.h>
#include <time.h>
#include <stdlib.h>

bool stop_threads = false;

pthread_mutex_t init_lock, print_lock;

typedef struct Thread_args{
    int id;
} Thread_args;

void* thread_func(void* in){
    static int cur_id = 0;
    pthread_mutex_lock(&init_lock);
    int id = cur_id;
    cur_id++;
    pthread_mutex_unlock(&init_lock);
    while(!stop_threads){
        pthread_mutex_lock(&print_lock);
        for (int i = 0; i < 10 ; i++){
            printf("thread %d says %d\n", id, i);
            sleep(rand() % 2 + 1);
        }
        pthread_mutex_unlock(&print_lock);
        sleep(rand() % 2 + 1);
    }
    pthread_exit(NULL);
}

int main(int argc, char** argv){
    srand(time(NULL));
    pthread_mutex_init(&init_lock, NULL);
    pthread_mutex_init(&print_lock, NULL);
    
    const int N_THREADS = 10;
    pthread_t threads[N_THREADS];
    for (int i = 0; i < N_THREADS; i ++){
        static int cur_t_id = 0;
        pthread_create(&threads[i], NULL, thread_func, NULL);
    }
    for (int i = 0; i < N_THREADS; i++){
        pthread_join(threads[i], NULL);
    }
}