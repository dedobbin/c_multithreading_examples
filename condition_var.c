#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condition_variable = PTHREAD_COND_INITIALIZER;
int condition_met = 0;

void* thread_function(void* arg) {
    printf("Thread waiting...\n");
    pthread_mutex_lock(&mutex);
    while (!condition_met) {
        printf("will wait..\n");
        pthread_cond_wait(&condition_variable, &mutex);
    }
    printf("Thread woke up!\n");
    pthread_mutex_unlock(&mutex);
    return NULL;
}

int main() {
    pthread_t thread;
    pthread_create(&thread, NULL, thread_function, NULL);
    sleep(2); // Sleep for 2 seconds to ensure the thread is waiting

    pthread_mutex_lock(&mutex);
    condition_met = 1; // Set the condition to true
    pthread_cond_signal(&condition_variable); // Wake up one waiting thread
    pthread_mutex_unlock(&mutex);

    pthread_join(thread, NULL);

    return 0;
}